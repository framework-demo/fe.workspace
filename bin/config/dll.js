'use strict';
// node_modules
const webpack = require('webpack');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

function webpackConfigFactory({ mode = 'dev', outDir}) {
    const isProd = mode !== 'dev';
    const ifDev = !isProd;
    const ProdPlugins = isProd ? [
        new webpack.LoaderOptionsPlugin({
          minimize: true,
          debug: false,
        }),
        new webpack.optimize.UglifyJsPlugin({
          compress: {
            screw_ie8: true,
            warnings: false,
          },
        }),
        new webpack.optimize.MinChunkSizePlugin({minChunkSize: 10000}),
        new webpack.optimize.OccurrenceOrderPlugin()
    ] : [];
   
    return Object.assign({}, {
        target: 'web',
        devtool: ifDev? 'cheap-eval-source-map': 'hidden-source-map',
        entry: {
            vendor: ['react','react-dom']
        },
        output: {
            path: outDir,
            filename: ifDev? '[name].js':'[name].js',
            chunkFilename: '[name]-[chunkhash:2].js',
            library: '[name]'
        },
        performance: {
            hints: false
        },
        module: {
            rules: [
                {
                    test    : /\.js$/,
                    loader  : 'babel-loader',
                    exclude : /node_modules/,
                    options: {
                        presets: [
                            'react',
                            'es2015'
                        ],
                        plugins: ['add-module-exports', 'transform-es2015-modules-commonjs']
                    }
                }
            ]
        },
        plugins: [
            new ProgressBarPlugin(),
            new webpack.DllPlugin({
                path: `${outDir}/[name]-manifest.json`,
                name: '[name]'
            }),
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': ifDev? '"dev"': '"production"'
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                minChunks: Infinity,
            }),
            ...ProdPlugins
        ],
        resolve: {
            extensions: ['.js']
        },
    })
}

module.exports = webpackConfigFactory;
