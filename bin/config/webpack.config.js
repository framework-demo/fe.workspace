let webpack = require('webpack')
let path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");



function hotEntry(entrys, addFiles, mode = 'dev') {
    if (mode === 'prod') {
        return entrys;
    }
    return Object.keys(entrys).reduce((objects, key) => {
        let filePath = entrys[key];

        if (filePath instanceof Array) {
            objects[key] = [
                ...addFiles,
                ...filePath
            ]
        } else {
            objects[key] = [
                ...addFiles,
                filePath
            ]
        }

        return objects;
    }, {})
}


module.exports = function (env) {
    
    const PackMode = env.PackMode;
    console.log('当前打包模式为：' + PackMode);

    function ifDev(a, b) {
        if (PackMode === 'dev' || PackMode === 'test') return a; else return b;
    }

    let configByMode = require(`./${ifDev('dev','prod')}.config`); // 获取对应配置文件



    return Object.assign({},{
        entry: {
            main: './src/entry.js',
            vendor: 'moment'
        },
        output: {
            path: "./dist",
            filename: ifDev('[name].js', '[name]-[hash:6].js'),
            publicPath: '/',
            chunkFilename: 'js/m-[chunkhash:6].js'
        },
        resolve: {
            modules: [
                "./src",
                "./node_modules"
            ]
        },
        devtool: ifDev('source-map', 'hidden-source-map'),
        module: {
            rules: [
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: "css-loader",
                        publicPath: "/dist"
                    })

                },
                {
                    test: /\.js?$/,
                    loader: "babel-loader",
                    options: {
                        presets: ['es2015'],
                        plugins: ['transform-runtime']
                    }
                },
                {
                    test: /\.s[a|c]ss$/,
                    loaders: ifDev(
                        ['style-loader', 'raw-loader', 'sass-loader?sourceMap'],
                        ExtractTextPlugin.extract({
                            use: ['raw-loader', 'postcss-loader', 'sass-loader']
                        })),
                    // use: [
                    //     "style-loader",
                    //     "css-loader",
                    //     "sass-loader"
                    // ]
                },
                {
                    test: /\.(png|jpg)$/,
                    use: [
                        "url-loader"
                    ]
                }
            ]
        },
        plugins: [
            ...((PackMode === 'prod' || PackMode === 'test') ? [new webpack.optimize.UglifyJsPlugin({
                sourceMap: options.devtool && (options.devtool.indexOf("sourcemap") >= 0 || options.devtool.indexOf("source-map") >= 0),
                minimize: true,
                compress: {
                    warnings: true
                }
            })] : []),
            // new webpack.BannerPlugin({ banner: 'Banner', raw: true, entryOnly: true }),
            new ExtractTextPlugin({
                filename: "bundle.css",
                disable: false,
                allChunks: true
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: ['vendor', 'manifest'] // Specify the common bundle's name.
            }),
            new webpack.HotModuleReplacementPlugin(),
            // enable HMR globally

            new webpack.NamedModulesPlugin(),
        ]
        // //其它解决方案配置
        // resolve: {
        //     // root: '__dirname/node_modul', //绝对路径
        //     extensions: ['', '.js', '.json', '.scss', '.vue'],
        //     alias: {
        //         //'Vue': 'vue/dist/vue.js'
        //     }
        // },
        // plugins: [
        //     new webpack.LoaderOptionsPlugin({
        //         // test: /\.xxx$/, // may apply this only for some modules
        //         options: {
        //             babel: ''
        //         }
        //     })
        // ]
    },configByMode)
}