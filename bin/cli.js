'use strict';

const program = require('commander');
const devServer = require('./main');
program
    .version('1.0.0')

program
    .command('dev')
    .description('Run a development environment')
    .action(function packAction() {
        devServer.doDevelop()
    });

program
    .command('test')
    .description('Pack a code test')
    .action(function packAction() {
        devServer.doPublishTest()
    });

program
    .command('prod')
    .description('Pack a code online')
    .action(function packAction() {
        devServer.doPublishOnline()
    })

program.parse(process.argv);

if (!program.args.length) {
    program.help();
}