


module.exports = Object.assign({}, {
    "entry": [
        'react-hot-loader/patch',
        // activate HMR for React

        'webpack-dev-server/client?http://localhost:8080',
        // bundle the client for webpack-dev-server
        // and connect to the provided endpoint

        'webpack/hot/only-dev-server',
        // bundle the client for hot reloading
        // only- means to only hot reload for successful updates

        './src/entry.js'
        // the entry point of our app
    ],
    devServer: {
        hot: true,
        // enable HMR on the server

        contentBase: './dist',
        // match the output path

        publicPath: './dist',
        // match the output `publicPath`

        port: 8090
    },
})