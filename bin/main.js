
const outDir = './dist';

require('shelljs/global');
const notifier = require('node-notifier');
const webpack = require('webpack');
const notifie = (TheTitle, message, open) => {
    const title = `${TheTitle.toUpperCase()}`;
    notifier.notify({
        title,
        message,
        open,
        sound: true
    });

    console.log(`==> ${title} -> ${message}`);
}
const logStats = stats => {
    console.log(stats.toString({
        assets: true,
        colors: true,
        version: true,
        hash: true,
        timings: true,
        chunks: true,
        chunkModules: false
    }))
}
const createWebpackCompiler = (config) => {
    let compiler;
    try {
        compiler = webpack(config);
    } catch (err) {
        console.log(err);
    }
    return compiler;
}

const compileDll = (cb, mode = 'dev') => {
    notifie('🎾 vendor DLL', 'Running');
    const config = require('./config/dll')({ mode, outDir });
    const compiler = createWebpackCompiler(config);
    compiler.run((err, stats) => {
        if ( stats.hasErrors() ) {
            notifie('💢❌‼️ vendor DLL', 'Build failed, check console for error');
            logStats(stats)
        } else {
            notifie('☀️ vendor DLL', 'Built!');
            logStats(stats);
            if (cb) cb(mode);
        }
    });
}

const compileClient = (mode = 'dev') => {
    notifie('🎾 client modules', 'Running');
    
    const config = require('../@core/config/webpack/client')({ mode, outDir })

    if ( mode === 'dev' ) {
        startService(config);
        notifie('🌎 client Watch ...', `Running on http://localhost:${env.clientPort}`);
    } else { // production
        const compiler = createWebpackCompiler(config);
        compiler.run((err, stats) => {
            if ( stats.hasErrors() ) {
                notifie('💢❌‼️ client modules', 'Build failed, check console for error');
                logStats(stats)
            } else {
                notifie('☀️ client modules', 'Built!');
                logStats(stats);
            }
        })
    }
}



// 开发模式
function doDevelop(){
    rm('-rf', outDir);
    mkdir(outDir);
    compileDll();
}

// 发布到测试环境
function doPublishTest(){
    rm('-rf', outDir);
    mkdir(outDir);
    compileDll();
}

// 发布上线
function doPublishOnline(){
    rm('-rf', outDir);
    mkdir(outDir);
}


module.exports = {
    doDevelop,
    doPublishTest,
    doPublishOnline
}